#!/bin/bash

cat <<EOF > child.yml
stages:
  - test

test_1:
  stage: test
  script:
    - echo "Hello world"
  when: manual

test_2:
  stage: test
  script:
    - echo "Hello world"
  when: manual
EOF
